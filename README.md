Automation Skeleton
===================

This is an example project skeleton that can be used for more complicated automation projects. It provides some
example classes that can be used as templates or examples for structuring your own projects. 

The classes in this project are broken up into different layers, each with it's own package directory:

        +---------------------+                                                         
        |       scripts       |                                                         
        +---------------------+                                                         
        +---------------------+                                                         
        |     experiments     |                                                         
        +---------------------+                                                         
        +---------------------+                                                         
        |      managers       |                                                         
        +---------------------+                                                         
        +---------------------+                                                         
        |        deck         |                                                         
        +---------------------+                                                         
        +---------------------+                                                         
        |     components      |                                                         
        +---------------------+                                                         
                                                                                
Scripts are the entry point to your automation project and will configure and instantiate classes from the `experiments` 
package. These `experiments` classes should only use classes from the `managers` package, and avoid using the `deck` or
`components` directly. The `deck` package contains classes that instantiate and organize components and other objects into
"decks", which represent everything available to the automation platform. These `deck` instances are used by classes in
the `managers` package to actually move and coordinate actions between everything on the deck.