class ExampleComponentA:
    def do_something(self, parameter: float):
        print(f'ExampleComponentA do_something({parameter})')
