class ExampleComponentB:
    def do_something(self, parameter: float):
        print(f'ExampleComponentB do_something({parameter})')
