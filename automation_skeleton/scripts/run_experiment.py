from automation_skeleton.configuration.deck import ExampleDeck
from automation_skeleton.experiments.example_experiment import ExampleExperiment

PARAMETER_A = 1.0
PARAMETER_B = 2


experiment = ExampleExperiment(ExampleDeck(), PARAMETER_A, PARAMETER_B)
experiment.run()
