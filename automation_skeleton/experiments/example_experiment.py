from automation_skeleton.configuration.deck import ExampleDeck


class ExampleExperiment:
    def __init__(self, deck: ExampleDeck, parameter_a: float, parameter_b: int):
        self.deck = deck
        self.parameter_a = parameter_a
        self.parameter_b = parameter_b

    def run(self):
        self.deck.example_manager.do_something(self.parameter_a)
        pass
