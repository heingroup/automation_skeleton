from automation_skeleton.components.example_component_a import ExampleComponentA
from automation_skeleton.components.example_component_b import ExampleComponentB
from automation_skeleton.managers.example_manager import ExampleManager


class ExampleDeck:
    def __init__(self):
        self.example_component_a = ExampleComponentA()
        self.example_component_b = ExampleComponentB()

        self.example_manager = ExampleManager(self.example_component_a, self.example_component_b)
