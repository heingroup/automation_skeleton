from automation_skeleton.components.example_component_a import ExampleComponentA
from automation_skeleton.components.example_component_b import ExampleComponentB


class ExampleManager:
    def __init__(self, example_component_a: ExampleComponentA, example_component_b: ExampleComponentB):
        self.example_component_a = example_component_a
        self.example_component_b = example_component_b

    def do_something(self, parameter: float):
        print(f'ExampleManager do_something({parameter})')
        self.example_component_a.do_something(parameter)
        self.example_component_b.do_something(parameter)
